Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_scope :user do
    root to: "devise/sessions#new"
  end
  
  resources :social_accounts, only: :index do
    collection do
      put 'search_accounts'
      get 'avaliable_business_accounts'
    end
  end
end
