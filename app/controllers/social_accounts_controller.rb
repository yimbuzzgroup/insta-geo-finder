class SocialAccountsController < ApplicationController

  def index
  end

  def search_accounts
    url = set_base_api_url
    page_name = params[:page_name] rescue ''
    search = "#{set_base_api_url}search?access_token=EAATZBhW2WqcMBAARQ69vfTdWLcDnoWDWrU1s6r6U4XtoNADqmPobJ2WEKk5wLSlc58LP6yjZBz7eWnJnUKFAxxoVs73TLLkJQF3rVCsJHDrdldLUtxNlEPxWbB2Atgjzoft1gPcn4WG0hIQgFOAjc9HyZA3QMTKwB0J9nJa8lWhymTIUxyZBkzeyxTOkeou8Hm7pIGnkngZDZD&q=#{page_name}&type=page"
    response = HTTParty.get(search)
    if response.present? && response.parsed_response['data'].present?
      response = response.parsed_response['data']
      facebook_page_ids = response.map{|f| f['id']}
      facebook_page_ids.each do |facebook_page_id|
        address_url = "#{set_base_api_url}/#{facebook_page_id}?access_token=EAATZBhW2WqcMBAARQ69vfTdWLcDnoWDWrU1s6r6U4XtoNADqmPobJ2WEKk5wLSlc58LP6yjZBz7eWnJnUKFAxxoVs73TLLkJQF3rVCsJHDrdldLUtxNlEPxWbB2Atgjzoft1gPcn4WG0hIQgFOAjc9HyZA3QMTKwB0J9nJa8lWhymTIUxyZBkzeyxTOkeou8Hm7pIGnkngZDZD&fields=location"
        address = HTTParty.get(address_url)
        if address.parsed_response.present? && address.parsed_response["location"].present?
          latitude = address.parsed_response["location"]["latitude"] rescue ''
          longitude = address.parsed_response["location"]["longitude"] rescue ''
          search_url = "#{set_base_api_url}search?access_token=EAATZBhW2WqcMBAARQ69vfTdWLcDnoWDWrU1s6r6U4XtoNADqmPobJ2WEKk5wLSlc58LP6yjZBz7eWnJnUKFAxxoVs73TLLkJQF3rVCsJHDrdldLUtxNlEPxWbB2Atgjzoft1gPcn4WG0hIQgFOAjc9HyZA3QMTKwB0J9nJa8lWhymTIUxyZBkzeyxTOkeou8Hm7pIGnkngZDZD&q=''&type=page&type=place&center=#{latitude},#{longitude}&distance=16093"
          response_on_location = HTTParty.get(search_url)
          if response_on_location.present? && response_on_location.parsed_response['data'].present?
            facebook_pages_on_location = response_on_location.parsed_response['data']
            facebook_page_ids = response.map{|f| f['id']}
            facebook_page_ids.each do |facebook_page_id|
              insta_account_url = "#{set_base_api_url}/#{facebook_page_id}?access_token=EAATZBhW2WqcMBAARQ69vfTdWLcDnoWDWrU1s6r6U4XtoNADqmPobJ2WEKk5wLSlc58LP6yjZBz7eWnJnUKFAxxoVs73TLLkJQF3rVCsJHDrdldLUtxNlEPxWbB2Atgjzoft1gPcn4WG0hIQgFOAjc9HyZA3QMTKwB0J9nJa8lWhymTIUxyZBkzeyxTOkeou8Hm7pIGnkngZDZD&fields=instagram_business_account"
              business_response = HTTParty.get(insta_account_url)
              business_response = business_response.parsed_response["instagram_business_account"] rescue ''
              if business_response.present?
                insta_account_id = business_response["id"] 
                insta_business_account_detail_url = "#{set_base_api_url}/#{insta_account_id}?access_token=EAATZBhW2WqcMBAARQ69vfTdWLcDnoWDWrU1s6r6U4XtoNADqmPobJ2WEKk5wLSlc58LP6yjZBz7eWnJnUKFAxxoVs73TLLkJQF3rVCsJHDrdldLUtxNlEPxWbB2Atgjzoft1gPcn4WG0hIQgFOAjc9HyZA3QMTKwB0J9nJa8lWhymTIUxyZBkzeyxTOkeou8Hm7pIGnkngZDZD&fields=biography,id,username,website,ig_id,followers_count,media_count,name,profile_picture_url" if insta_account_id.present?
                insta_response = HTTParty.get(insta_business_account_detail_url)
                account_detail = insta_response.parsed_response rescue ''
                business_account = BusinessAccount.create(account_id: account_detail['id'], 
                                                          name: account_detail['name'],
                                                          profile_picture_url: account_detail['profile_picture_url'],
                                                          followers_count: account_detail['followers_count'],
                                                          media_count: account_detail['media_count'],
                                                          username: account_detail['media_count'],
                                                          ig_id: account_detail['ig_id'] )
              end
            end
          end
        end
      end
    end
    redirect_to avaliable_business_accounts_social_accounts_path
  end

  def avaliable_business_accounts
    @business_accounts = BusinessAccount.all
  end

  def set_base_api_url
    'https://graph.facebook.com/v2.10/'
  end
end