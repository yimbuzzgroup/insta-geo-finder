class BusinessAccount < ApplicationRecord
  validates :account_id, :username, uniqueness: true
end
