class CreateBusinessAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :business_accounts do |t|
      t.string :account_id
      t.string :name
      t.string :profile_picture_url
      t.integer :followers_count
      t.integer :media_count
      t.string :username
      t.string :ig_id
      t.timestamps
    end
  end
end
